var Particle = (function() {
  EasingFunctions = {
    // no easing, no acceleration
    linear: function (t) { return t },
    // accelerating from zero velocity
    easeInQuad: function (t) { return t*t },
    // decelerating to zero velocity
    easeOutQuad: function (t) { return t*(2-t) },
    // acceleration until halfway, then deceleration
    easeInOutQuad: function (t) { return t<.5 ? 2*t*t : -1+(4-2*t)*t },
    // accelerating from zero velocity
    easeInCubic: function (t) { return t*t*t },
    // decelerating to zero velocity
    easeOutCubic: function (t) { return (--t)*t*t+1 },
    // acceleration until halfway, then deceleration
    easeInOutCubic: function (t) { return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1 },
    // accelerating from zero velocity
    easeInQuart: function (t) { return t*t*t*t },
    // decelerating to zero velocity
    easeOutQuart: function (t) { return 1-(--t)*t*t*t },
    // acceleration until halfway, then deceleration
    easeInOutQuart: function (t) { return t<.5 ? 8*t*t*t*t : 1-8*(--t)*t*t*t },
    // accelerating from zero velocity
    easeInQuint: function (t) { return t*t*t*t*t },
    // decelerating to zero velocity
    easeOutQuint: function (t) { return 1+(--t)*t*t*t*t },
    // acceleration until halfway, then deceleration
    easeInOutQuint: function (t) { return t<.5 ? 16*t*t*t*t*t : 1+16*(--t)*t*t*t*t }
  }

  function Particle(start, end, step, easingFunction, image, size) {
    this.easingFunction = easingFunction ? easingFunction : 'easeOutQuad';
    this.x = this.oldX = start.x;
    this.y = this.oldY = start.y;
    this.start = start;
    this.end = end;
    this.t = 0;
    this.step = step;
    this.size = this.startSize = size;
    this.image = image;
  }
  Particle.prototype.integrate = function() {
    if (this.t < 1) {
      var stepsAgo = Math.max(this.t - 5 * this.step, 0);
      this.oldX = (this.end.x - this.start.x) * EasingFunctions[this.easingFunction](stepsAgo) + this.start.x
      this.oldY = (this.end.y - this.start.y) * EasingFunctions[this.easingFunction](stepsAgo) + this.start.y;
      this.x = (this.end.x - this.start.x) * EasingFunctions[this.easingFunction](this.t) + this.start.x;
      this.y = (this.end.y - this.start.y) * EasingFunctions[this.easingFunction](this.t) + this.start.y;
      this.t += this.step;
    }
  };
  Particle.prototype.draw = function(ctx) {
    ctx.strokeStyle = '#ffffff';
    ctx.lineWidth = 1;
    ctx.beginPath();
    ctx.moveTo(this.oldX, this.oldY);
    ctx.lineTo(this.x, this.y);
    ctx.stroke();
  };

  Particle.prototype.drawImage = function(ctx) {
    if (this.t < 1) {
      ctx.drawImage(this.image, this.x, this.y, this.size.x, this.size.y);
    }
  }

  return Particle;
})();
