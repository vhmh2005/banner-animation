var Firework = (function() {
  var display = document.getElementById('mainFireworkCanvas');
  var ctx = display.getContext('2d');
  var particles = [];
  var width = display.width;
  var height = display.height;
  var particleCount = 100;

  for (var i = 0; i < particleCount; i++) {
    var start = {x:190, y: 20};
    var angle = 2 * Math.PI  / particleCount * i;
    var distance =  Math.floor( Math.random() * 40);
    var end = {
      x:start.x + distance * Math.sin(angle) * 1.8,
      y:start.y + distance * Math.cos(angle)
    }
    particles.push(new Particle(start,end, 0.02,'easeOutQuint'));
  }

  function frame() {
    if (particles.length > 0) {
      requestAnimationFrame(frame);
      ctx.clearRect(0, 0, width, height);
      var i = particles.length;
      while (i--) {
        particles[i].integrate();
        particles[i].draw(ctx);
      }
    }
  }

  setTimeout(requestAnimationFrame.bind(this,frame), 2000);
})();
