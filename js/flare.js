var Flare = (function(){
  var display = document.getElementById('flareCanvas');
  var ctx = display.getContext('2d');
  var particles = [];
  var width = display.width;
  var height = display.height;
  var particleCount = 10;
  var img = document.getElementById('flareImage');
  ctx.globalCompositeOperation = "lighter";
  ctx.globalAlpha = 1;

  for (var i = 0; i < particleCount; i++) {
    var xDistance = Math.floor(Math.random() * 80) - 40;
    var yDistance = Math.floor(Math.random() * 60) - 30;
    start = {
      x : 180,
      y : 10
    };
    end = {
      x : start.x + xDistance,
      y : start.y + yDistance
    };
    var sizeX = Math.floor(Math.random() * 50);
    size = {
      x: sizeX,
      y: sizeX
    }
    var step = Math.max(Math.random() / 20, 0.02);
    particles.push(new Particle(start, end, step, 'easeOutQuad', img, size));
  }

  function frame() {
    if (particles.length > 0) {
      requestAnimationFrame(frame);
      ctx.clearRect(0, 0, width, height);
      var i = particles.length;
      while (i--) {
        particles[i].integrate();
        particles[i].drawImage(ctx);
      }
    }
  }

  setTimeout(requestAnimationFrame.bind(this,frame), 2100);
})();
