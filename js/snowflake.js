var Snowflake = (function(){
  var display = document.getElementById('snowflakeCanvas');
  var ctx = display.getContext('2d');
  var particles = [];
  var width = display.width;
  var height = display.height;
  var particleCount = 100;
  var img = document.getElementById('snowflakeImage');
  var imgSmall = document.getElementById('snowflakeSmallImage');
  ctx.globalCompositeOperation = "lighter";
  ctx.globalAlpha = 0.5;

  // biggest snowflake
  var start = {x:-10, y: 100};
  var end = {x:350, y: 100};
  var size = {x: 100, y:80};
  particles.push(new Particle(start,end, 0.05,'linear', img, size));

  // other smaller random snowflake
  for (var i = 0; i < particleCount; i++) {
    var xDistance = Math.floor(Math.random() * 100);
    var yDistance = Math.floor(Math.random() * 80);
    start = {
      x : Math.floor(width/particleCount * i),
      y : 150
    };
    end = {
      x : start.x + xDistance,
      y : start.y - yDistance
    };
    var sizeX = Math.floor(Math.random() * 30);
    size = {
      x: sizeX,
      y: Math.floor(sizeX * 4 / 5)
    }
    var step = Math.max(Math.random() / 20, 0.003);
    particles.push(new Particle(start, end, step, 'easeOutQuad', imgSmall, size));
  }

  function frame() {
    if (particles.length > 0) {
      requestAnimationFrame(frame);
      ctx.clearRect(0, 0, width, height);
      var i = particles.length;
      while (i--) {
        particles[i].integrate();
        particles[i].drawImage(ctx);
      }
    }
  }

  setTimeout(requestAnimationFrame.bind(this,frame), 3500);
})();
